/*
| Starts the server
*/
'use strict';

Error.stackTraceLimit = 15;
//Error.stackTraceLimit = Infinity;
process.on( 'unhandledRejection', err => { throw err; } );

/*
| Sets root as global variable.
| Works around to hide node.js unnessary warning.
*/
Object.defineProperty(
	global, 'root',
	{ configureable: true, writable: true, enumerable: true, value: undefined }
);

// this is node.
global.NODE = true;

// server checking (first true, later override by config)
global.CHECK = true;

// server isn't a visual
global.VISUAL = false;

const pkg =
	require( '@timberdoodle/tim' )
	.register( 'testpad', module, 'src/', 'Server/Start.js' );

// timberdoodle packages
require( '@timberdoodle/timberman' );

pkg.require( 'Server/Root' ).init( pkg.dir );
