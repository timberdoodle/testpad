/*
| The root of the server.
*/
'use strict';

def.attributes =
{
	// absolute path of the plotle directory
	dir: { type: 'tim:Path' },

	// the servers tim middleware
	timberman: { type: 'Server/Timberman' },
};

def.global = 'root';

const util = require( 'util' );
const http = require( 'http' );

const Timberman = tim.require( 'Server/Timberman' );

/*
| Initializes server root.
|
| ~dir: dir of testpad
*/
def.static.init =
	async function( dir )
{
	Self.create( 'dir', dir, 'timberman', await Timberman.prepare( dir ) );
	await Self._startMainServer( );
	console.log( 'testpad running' );
};

/*
| Starts the main server.
*/
def.static._startMainServer =
	async function( )
{
	const listen = null;
	const port = 8834;

	const handler =
		( request, result ) =>
	{
		root.timberman.requestListener( request, result )
		.catch( ( error ) => { console.error( error ); process.exit( -1 ); } );
	};

	console.log( 'starting server @ http://' + ( listen || '*' ) + '/:' + port );
	const server = http.createServer( handler );
	const promise = util.promisify( server.listen.bind( server ) );
	await promise( port, listen );
};
