/*
| Handles plotle specific adaptions to timberman.
*/
'use strict';

def.attributes =
{
	// the general timberman instance
	_tm: { type: 'timberman:timberman' }
};

const timberman = tim.require( 'timberman:timberman' );

tim.require( 'Testpad/Root' );

/*
| To be called by timberman when a resource file changes.
|
| ~timberman:  timbernam instance of the watch registration
| ~name:       name of the resource changed
| ~eventType:  eventType
*/
const notify =
	function( timberman, name, eventType )
{
	console.log( 'Resource ' + name + ' file event; rebuilding timberman' );
	// rebuilding timberman
	Self.rebuildTimberman( name )
	.catch( ( error ) => { console.error( error ); process.exit( -1 ); } );
};

/*
| Adds the basic roster to timberman.
*/
def.static.addBasics =
	async function( timberman, base )
{
	return await timberman.addResource(
		base,
		{
			name : [ 'index.html', '' ],
			file : './media/index.html',
			age : 'short',
		},
	);
};

/*
| Prepares timberman.
| Also builds the bundle.
*/
def.static.prepare =
	async function( adir )
{
	console.log( 'preparing timberman' );
	let tm = timberman.create( 'notify', notify, 'requestCaching', false );
	tm = await Self._addGlobals( tm );
	// prepares resources from the roster
	tm = await tim.addTimbermanResources( tm, [ 'shell', 'testpad' ] );
	tm = await Self.addBasics( tm, adir );
	// prepares resources form the the shell
	tm = await tm.addCopse( 'testpad/Testpad/Root.js', 'testpad' );
	// adds the tim catalog init
	tm = tim.addTimbermanCatalog( tm );
	// transducing
	tm = Self._testpadHtml( tm );
	return Self.create( '_tm', tm );
};


/*
| Rebuilds the timberman incase of dev updates.
|
| ~name: resource name that trigered.
*/
def.static.rebuildTimberman =
	async function( name )
{
	// for now there are some memory issues on hot rebuilds
	process.exit( 1 );
	const tm = root.timberman._tm;
	const res = tm.get( name );
	if( res )
	{
		delete require.cache[ res.afile.asString ];
		require( res.afile.asString );
	}
	root.create( 'timberman', await Self.prepare( root.adir ) );
};

/*
| Listens to http requests
| FIXME remove
*/
def.proto.requestListener =
	async function( request, result )
{
	return this._tm.requestListener( request, result );
};

/*
| Builds the shell bundle.
*/
def.static._buildBundle =
	function( timberman )
{
	console.log( 'concating bundle' );
	let code = '';
	for( let name of timberman.getList( 'shell' ) )
	{
		const res = timberman.get( name );
		code += res.data + '';
	}
	return Object.freeze( { code: code } );
};

/*
| Applies all transducing.
|
| ~timberman:  the timberman to process
| ~bundleName: name of the bundle
*/
def.static._transduce =
	function( timberman )
{
	return Self._testpadHtml( timberman );
};

/*
| The shell's globals as resource.
|
| ~timberman:
*/
def.static._addGlobals =
	async function( timberman )
{
	let text = '';
	const globals = Self._shellGlobals( );
	for( let key of Object.keys( globals ).sort( ) )
	{
		text += 'var ' + key + ' = ' + globals[ key ] + ';\n';
	}

	return(
		await timberman.addResource(
			undefined,
			{
				name : 'global-testpad.js',
				data : text,
				list : 'testpad'
			}
		)
	);
};

/*
| Postprocessing for devel.html.
|
| ~timberman:    timberman to transduce
| ~hashOpentype: hash of opentype.js
*/
def.static._develHtml =
	function( timberman, hashOpentype )
{
/**/if( CHECK && arguments.length !== 2 ) throw new Error( );

	const res = timberman.get( 'devel.html' );
	const scripts = [ ];
	for( let name of timberman.getList( 'shell' ) )
	{
		scripts.push( '<script src="' + name + '" type="text/javascript" defer></script>' );
	}

	let data = res.data + '';
	data = data.replace( /<!--SCRIPTS.*>/, scripts.join( '\n' ) );
	data = data.replace(
		/<!--OPENTYPE.*>/,
		'<script src="opentype-' + hashOpentype + '.js" type="text/javascript" defer></script>'
	);

	return timberman.updateResource( res.create( 'data', data ) );
};

/*
| Postprocessing for index.html.
|
| ~timberman:       timberman to transduce
| ~bundleName:      name of the bundle
| ~hashOpentypeMin: hash of minified opentype.js
*/
def.static._indexHtml =
	function( timberman, bundleName, hashOpentypeMin )
{
/**/if( CHECK && arguments.length !== 3 ) throw new Error( );

	const res = timberman.get( 'index.html' );
	let data = res.data + '';

	data =
		data
		.replace(
			/<!--SCRIPTS.*>/,
			'<script src="' + bundleName + '" type="text/javascript"></script>'
		)
		.replace(
			/<!--OPENTYPE.*>/,
			'<script src="opentype.min-' + hashOpentypeMin + '.js" type="text/javascript"></script>'
		);

	return timberman.updateResource( res.create( 'data', data ) );
};

/*
| The shell's globals.
*/
def.static._shellGlobals =
	function( )
{
/**/if( CHECK && arguments.length !== 0 ) throw new Error( );

	return(
		Object.freeze( {
			CHECK: true,
			NODE: false,
			FAILSCREEN: false,
			VISUAL: false,
		} )
	);
};

/*
| PostProcessor for testpad.html
|
| ~timberman: timberman to transduce
*/
def.static._testpadHtml =
	function( timberman )
{
	const res = timberman.get( 'index.html' );
	const scripts = [ ];
	for( let name of timberman.getList( 'testpad' ) )
	{
		scripts.push( '<script src="' + name + '" type="text/javascript" defer></script>' );
	}
	let data = res.data + '';
	data = data.replace( /<!--SCRIPTS.*>/, scripts.join( '\n' ) );
	return timberman.updateResource( res.create( 'data', data ) );
};
