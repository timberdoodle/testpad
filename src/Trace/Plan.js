/*
| Plan of traces.
*/
'use strict';


def.abstract = true;

const Note = tim.require( 'Fabric/Note' );
const Para = tim.require( 'Fabric/Para/Self' );
const Plan = tim.require( 'tim:Plan' );
const Text = tim.require( 'Fabric/Text' );

def.staticLazy.item = ( ) =>
	Plan.Build( {
		name: 'item',
		key: true,
		subs:
		{
			text:
			{
				subs:
				{
					paras:
					{
						at: true,
						subs:
						{
							string: { offset: { at: true } },
						},
						types: [ Para ],
					},
				},
				types: [ Text ],
			},
		},
		types: [ Note ],
	} );

