/*
| A testing pad for the CC/OT engine.
*/
'use strict';

def.attributes =
{
	// the action the user is preparing
	action: { type: [ 'undefined', 'Testpad/Action' ] },

	// removes the beep
	beepTimer: { type: [ 'undefined', 'number' ] },

	// offset cursor is at
	cursorAt: { type: 'integer', defaultValue: '0' },

	// line cursor is in
	cursorLine: { type: 'integer', defaultValue: '0' },

	// DOM elements
	elements: { type: 'protean' },

	// true when having focus
	haveFocus: { type: 'boolean', defaultValue: 'false' },

	// true when mouse button is held down
	mouseDown: { type: 'boolean', defaultValue: 'false' },

	// the testing repository
	repository:
	{
		type: 'Testpad/Repository',
		defaultValue: 'require("Testpad/Repository").init( )'
	},
};

def.global = 'root';

const Action = tim.require( 'Testpad/Action' );
const ChangeList = tim.require( 'tim:Change/List' );
const ChangeWrap = tim.require( 'tim:Change/Wrap' );
const EMath = tim.require( 'Math/Self' );
const Plan = tim.require( 'Trace/Plan' );
const TextInsert = tim.require( 'tim:Change/Text/Insert' );
const TextJoin = tim.require( 'tim:Change/Text/Join' );
const TextRemove = tim.require( 'tim:Change/Text/Remove' );
const TextSplit = tim.require( 'tim:Change/Text/Split' );
const Trace = tim.require( 'tim:Trace' );
const Uid = tim.require( 'tim:Uid' );

/*
| Path to the text.
*/
def.staticLazy.textTrace = ( ) => Trace.root( Plan.item ).add( 'text' );

/*
| Binds an event handler to the
| latest instance of testpads root.
| ~handler: the handler of testpad
*/
const _bind =
	function( handler )
{
	return( function( ) { root[ handler ].apply( root, arguments ); } );
};

/*
| Returns true if a keyCode is known to be a "special key".
*/
def.staticLazy._specialKeys = ( ) =>
	new Set( [
		8,    // backspace
		13,   // return
		27,   // esc
		35,   // end
		36,   // pos1
		37,   // left
		38,   // up
		39,   // right
		40,   // down
		46,   // del
	] );

/*
| Alters the tree.
*/
def.proto.alter =
	function( change )
{
	const changeWrap =
		ChangeWrap.create(
			'cid', Uid.newUid( ),
			'changeList', ChangeList.create( 'list:set', 0, change )
		);
	root.repository.alter( changeWrap );
};

/*
| Shortcut to text.
*/
def.lazy.text = function( ) { return this.repository.note.text; };

/*
| Updates the sequence number
*/
def.proto.updateSeq =
	function( seq )
{
	const repository = this.repository;
	seq = EMath.limit( 0, seq, repository.maxSeq );
	return root.create( 'repository', repository.create( 'seq', seq ) );
};

/*
| Updates the html elements after altering the root.
*/
def.proto.update =
	function( )
{
	const elements = root.elements;
	elements.send.disabled = elements.cancel.disabled = !root.action;
	elements.now.innerHTML = '' + root.repository.seq;
	const text = root.text;
	const cursorLine = EMath.limit( 0, root.cursorLine, text.paras.length - 1 );
	const cursorAt = EMath.limit( 0, root.cursorAt, text.paras.get( cursorLine ).string.length );
	elements.pad.innerHTML =
		Self._makeScreen( text, this.action, this.haveFocus, cursorLine, cursorAt );
	root.create(
		'elements', elements,
		'cursorLine', cursorLine,
		'cursorAt', cursorAt
	);
};

/*
| Mouse down event on pad -> focuses the hidden input,
*/
def.proto.onMouseDown =
	function( event )
{
	if( event.button !== 0 ) return;
	event.preventDefault( );
	root.captureEvents( );
	root.create( 'mouseDown', true ).update( );
	root.elements.input.focus( );
	const pad = root.elements.pad;
	const measure = root.elements.measure;
	const text = root.text;
	const x = event.pageX - pad.offsetLeft;
	const y = event.pageY - pad.offsetTop;
	if( !text ) { root.beep( ); return; }
	const cLine = EMath.limit( 0, Math.floor( y / measure.offsetHeight ), text.paras.length - 1 );
	const cString = text.paras.get( cLine ).string;
	root.create(
		'cursorLine', cLine,
		'cursorAt', EMath.limit( 0, Math.floor( x / measure.offsetWidth ), cString.length )
	).update( );
};

/*
| Captures all mouse events.
*/
def.proto.captureEvents =
	function( )
{
	const pad = root.elements.pad;
	if( pad.setCapture ) pad.setCapture( pad );
	else
	{
		document.onmouseup = _bind( 'onMouseUp' );
		document.onmousemove = _bind( 'onMouseMove' );
	}
};

/*
| Stops capturing all mouse events.
*/
def.proto.releaseEvents =
	function( )
{
	const pad = this.elements.pad;
	if( pad.setCapture )
	{
		pad.releaseCapture( pad );
	}
	else
	{
		document.onmouseup = document.onmousemove = undefined;
	}
};

/*
| Mouse button released.
*/
def.proto.onMouseUp =
	function( event )
{
	if( event.button !==  0) return;
	event.preventDefault( );
	root.create( 'mouseDown', false ).update( );
	root.releaseEvents( );
};

/*
| Mouse clicked on pad.
*/
def.proto.onMouseClick = function( event) { event.preventDefault( ); };

/*
| Mouse moved over pad
| (or while dragging around it).
*/
def.proto.onMouseMove = function( event ) { if( root.mouseDown ) root.onMouseDown( event ); };

/*
| Key down event to ( hidden ) input.
*/
def.proto.onKeyDown =
	function( event )
{
	if( Self._specialKeys.has( event.keyCode ) )
	{
		event.preventDefault( );
		root.inputSpecialKey( event.keyCode, event.ctrlKey );
	}
	else root.testInput( );
};

/*
| Press event to (hidden) input.
*/
def.proto.onKeyPress =
	function( /* event */ )
{
	setTimeout( _bind( 'testInput' ), 0 );
};

/*
| Up event to (hidden) input.
*/
def.proto.onKeyUp =
	function( /* event */ )
{
	root.testInput( );
};

/*
| Hidden input got focus.
*/
def.proto.onFocus = function( ) { root.create( 'haveFocus', true ).update( ); };

/*
| Hidden input lost focus.
*/
def.proto.onBlur = function( ) { root.create( 'haveFocus', false ).update( ); };

/*
| Sends the current action.
*/
def.proto.send =
	function( )
{
	const action = this.action;
	let cursorAt;
	const text = this.text;
	if( !action ) { this.beep( ); return; }

	const lineTrace = Self.textTrace.add( 'paras', action.line );

	switch( action.command )
	{
		case 'insert' :
			root.alter(
				TextInsert.create(
					'val', action.value,
					'trace', lineTrace.add( 'string' ),
					'at1', action.at,
					'at2', action.at + action.value.length
				)
			);
			cursorAt = this.cursorAt + action.value.length;
			break;

		case 'remove' :
			root.alter(
				TextRemove.create(
					'val',
						text.paras.get( action.line ).string
						.substring( action.at2, action.at ),
					'trace', lineTrace.add( 'string' ),
					'at1', action.at,
					'at2', action.at2
				)
			);

			if( this.cursorLine === action.line && this.cursorAt >= action.at2 )
			{
				cursorAt = this.cursorAt - ( action.at2 - action.at );
			}
			break;

		case 'split' :
			root.alter(
				TextSplit.create(
					'trace', lineTrace.add( 'string' ),
					'at1', action.at
				)
			);
			break;

		case 'join' :
			root.alter(
				TextJoin.create(
					'trace',
						Self.textTrace
						.add( 'paras', action.line - 1 )
						.add( 'string' ),
					'at1', text.paras.get( action.line - 1 ).string.length
				)
			);
			break;
		default : throw new Error( );
	}

	root.create( 'action', undefined, 'cursorAt', cursorAt )
	.updateSeq( EMath.maxInteger )
	.update( );
};

/*
| Cancels the current action.
*/
def.proto.onCancelButton = function( ) { root.create( 'action', undefined ).update( ); };

/*
| Displays a beep message.
*/
def.proto.beep =
	function( )
{
	root.elements.beep.innerHTML = 'BEEP!';
	if( root.beepTimer ) clearInterval( root.beepTimer );
	root.create( 'beepTimer', setInterval( _bind( 'clearBeep' ), 540 ) ).update( );
};

/*
| Clears the beep message.
*/
def.proto.clearBeep =
	function( )
{
	root.elements.beep.innerHTML = '';
	clearInterval( root.beepTimer );
	root.create( 'beepTimer', undefined ).update( );
};

/*
| Aquires non-special input from (hidden) input.
*/
def.proto.testInput =
	function( )
{
	const action = root.action;
	const cursorLine = root.cursorLine;
	const cursorAt = root.cursorAt;
	const elements = root.elements;
	const text = elements.input.value;
	elements.input.value = '';
	if( text === '' ) return;
	if( !root.text ) { root.beep( ); return; }
	if( !action )
	{
		root.create(
			'action',
			Action.create( 'command', 'insert', 'line', cursorLine, 'at', cursorAt, 'value', text )
		).update( );
		return;
	}

	if( action.command === 'insert'
	&& cursorLine === action.line
	&& cursorAt === action.at
	)
	{
		root
		.create( 'action', action.create( 'value', action.value + text ) )
		.update( );
		return;
	}

	root.beep( );
};


/*
| Handles all kind of special keys.
*/
def.proto.inputSpecialKey =
	function( keyCode, ctrl )
{
	const action = root.action;
	const cursorLine = root.cursorLine;
	const cursorAt = root.cursorAt;
	const text = root.text;
	switch( keyCode )
	{
		case  8 : // backspace
			if( !text ) { root.beep( ); return; }
			if( cursorAt <= 0 )
			{
				if( action || cursorLine <= 0 ) { root.beep( ); return; }

				root.create(
					'action', Action.create( 'command', 'join', 'line', cursorLine )
				).update( );
				return;
			}

			if( !action )
			{
				root.create(
					'action',
						Action.create(
							'command', 'remove',
							'line', cursorLine,
							'at', cursorAt - 1,
							'at2', cursorAt
						),
					'cursorAt', cursorAt - 1
				).update( );
				return;
			}

			if( action.command !== 'remove' || cursorAt !== action.at )
			{
				root.beep( );
				return;
			}
			root.create(
				'action', root.action.create( 'at', root.action.at - 1 ),
				'cursorAt', cursorAt - 1
			).update( );
			return;
		case 13 : // return
			if( !text ) { root.beep( ); return; }
			if( ctrl ) { root.send( ); return; }
			if( action ) { root.beep( ); return; }
			root.create(
				'action',
					Action.create(
						'command', 'split',
						'line', cursorLine,
						'at', cursorAt
					)
			).update( );
			return;
		case 27 : // esc
			root.create( 'action', undefined ).update( );
			return;
		case 35 : // end
			if( !text ) { this.beep( ); return; }
			root.create( 'cursorAt', text.paras.atRank( cursorLine ).text.length ).update( );
			return;
		case 36 : // pos1
			if( !text ) { this.beep( ); return; }
			root.create( 'cursorAt', 0 ).update( );
			return;
		case 37 : // left
			if( !text ) { this.beep( ); return; }
			if( cursorAt <= 0 ) { this.beep( ); return; }
			root.create( 'cursorAt', cursorAt - 1 ).update( );
			return;
		case 38 : // up
			if( !text || cursorLine <= 0) { this.beep( ); return; }
			root.create( 'cursorLine', cursorLine - 1 ).update( );
			return;
		case 39 : // right
			if( !text ) { this.beep( ); return; }
			root.create( 'cursorAt', cursorAt + 1 ).update( );
			return;
		case 40 : // down
			if( !text || cursorLine >= text.paras.length - 1 ){ this.beep( ); return; }
			root.create( 'cursorLine', cursorLine + 1 ).update( );
			return;
		case 46 : // del
		{
			if( !text ) { this.beep( ); return; }
			const text = text.paras.atRank( cursorLine ).text;
			if( cursorAt >= text.length ) { this.beep( ); return; }
			if( !action )
			{
				root.create(
					'action',
						Action.create(
							'command', 'remove',
							'line', cursorLine,
							'at', cursorAt,
							'at2', cursorAt + 1
						),
					'cursorAt', cursorAt + 1
				).update( );
				return;
			}

			if( action.command !== 'remove' || cursorAt !== action.at2 )
			{
				this.beep( );
				return;
			}

			root.create(
				'action', action.create( 'at2', action.at2 + 1 ),
				'cursorAt', cursorAt + 1
			).update( );
			return;
		}
	}
};

/*
| Button update-to-now has been clicked.
*/
def.proto.onUpNowButton =
	function( )
{
	root.updateSeq( EMath.maxInteger ).update( );
	this.elements.input.focus( );
};

/*
| Button one-up-the-sequence has been clicked.
*/
def.proto.onUpButton =
	function( )
{
	root.updateSeq( root.repository.seq + 1 ).update( );
	this.elements.input.focus( );
};

/*
| Button one-down-the-sequence has been clicked.
*/
def.proto.onDownButton =
	function( )
{
	root.updateSeq( root.repository.seq - 1 ).update( );
	this.elements.input.focus( );
};

/*
| Cretes a screen for current data.
*/
def.static._makeScreen =
	function( text, action, haveFocus, cursorLine, cursorAt )
{
	const lines = [ ];
	// splits up the text into
	// an array of lines which are
	// an array of chars
	for( let para of text.paras ) lines.push( para.string.split( '' ) );
	// replaces HTML entities
	for( let line of lines )
	{
		for( let b = 0, blen = line.length; b < blen; b++ )
		{
			switch( line[ b ] )
			{
				case '&' : line[ b ] = '&amp;'; break;
				case '"' : line[ b ] = '&quot;'; break;
				case '<' : line[ b ] = '&lt;';  break;
				case '>' : line[ b ] = '&gt;';  break;
			}
		}
	}
	// inserts the cursor
	if( haveFocus )
	{
		const cText = lines[ cursorLine ];
		const cLen = lines[ cursorLine ].length;
		if( cursorAt >= cText.length )
		{
			cursorAt = cText.length;
			lines[ cursorLine ][ cursorAt ] = ' ';
		}
		lines[ cursorLine ][ cursorAt ] =
			'<span id="cursor">'
			+ lines[ cursorLine ][ cursorAt ]
			+ '</span>';
		if( cursorAt === cLen ) lines[ cursorLine ].push( ' ' );
	}

	// inserts the action
	switch( action && action.command )
	{
		case undefined :
			break;
		case 'join' :
			lines[ action.line ]
			.unshift( '<span id="join">↰</span>' );
			break;
		case 'split' :
			lines[ action.line ].splice( action.at, 0, '<span id="split">⤶</span>' );
			break;
		case 'insert' :
			lines[ action.line ]
			.splice( action.at, 0, '<span id="insert">', action.value, '</span>' );
			break;
		case 'remove' :
			if( action.at > action.at2 ) throw new Error( 'Invalid remove action' );
			lines[ action.line ].splice( action.at, 0, '<span id="remove">' );
			lines[ action.line ].splice( action.at2 + 1, 0, '</span>' );
			break;
		default :
			throw new Error( 'Unknown action.command: ' + action.command );
	}
	// transforms lines to a HTML string
	for( let a = 0, al = lines.length; a < al; a++ )
		lines[ a ] = lines[ a ].join( '' );
	return lines.join( '\n' );
};


/*
| Window.
*/
if( !NODE )
{
	window.onload =
		function( )
	{
		const elements =
		{
			measure : undefined,
			pad : undefined,
			input : undefined,
			beep : undefined,
			send : undefined,
			cancel : undefined,
			upnow : undefined,
			up : undefined,
			now : undefined,
			down : undefined
		};

		for( let id in elements ) elements[ id ] = document.getElementById( id );

		const pad = elements.pad;
		const input = elements.input;
		const send = elements.send;
		const cancel = elements.cancel;
		const down = elements.down;
		const up = elements.up;
		const upnow = elements.upnow;
		pad.onmousedown = _bind( 'onMouseDown' );
		pad.onmousemove = _bind( 'onMouseMove' );
		pad.onmouseup = _bind( 'onMouseUp' );
		pad.onclick = _bind( 'onMouseClick' );
		input.onkeypress = _bind( 'onKeyPress' );
		input.onkeydown = _bind( 'onKeyDown' );
		input.onkeyup = _bind( 'onKeyUp' );
		input.onfocus = _bind( 'onFocus' );
		input.onblur = _bind( 'onBlur' );
		send.onclick = _bind( 'send' );
		cancel.onclick = _bind( 'onCancelButton' );
		down.onclick = _bind( 'onDownButton' );
		up.onclick = _bind( 'onUpButton' );
		upnow.onclick = _bind( 'onUpNowButton' );

		Self.create( 'elements', elements ).update( );
		root.elements.input.focus( );
	};
}
