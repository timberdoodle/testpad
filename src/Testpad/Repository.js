/*
| The repository simulator does not talk to a server.
*/
'use strict';

def.attributes =
{
	// current sequence numer
	seq: { type: 'integer' },

	// history of all changes
	_changeWrapList: { type: [ 'undefined', 'tim:Change/Wrap/List' ] },

	// the note
	_note: { type: 'Fabric/Note' },
};

const ChangeWrapList = tim.require( 'tim:Change/Wrap/List' );
const Note = tim.require( 'Fabric/Note' );
const Para = tim.require( 'Fabric/Para/Self' );
const ParaList = tim.require( 'Fabric/Para/List' );
const Text = tim.require( 'Fabric/Text' );

/*
| Alters the tree.
| ~cw: changeWrap
*/
def.proto.alter =
	function( cw )
{
	const cwList = this._changeWrapList;
	const seq = this.seq;
	for( let s = seq, slen = cwList.length; s < slen; s++ )
	{
		cw = cwList.get( s ).transform( cw );
	}
	root.create(
		'repository',
			root.repository.create(
				'_changeWrapList', cwList.append( cw ),
				'_note', cw.changeTree( this._note )
			)
	);
};


/*
| Creates the start of the repository.
*/
def.static.init =
	function( )
{
	return(
		Self.create(
			'seq', 0,
			'_changeWrapList',  ChangeWrapList.create( ),
			'_note',
				Note.create(
					'text',
						Text.create(
							'paras',
								ParaList.Elements(
//									Para.create( 'string', 'a' ),
									Para.create( 'string', 'Ameno' ),
									Para.create( 'string', 'Latire' ),
									Para.create( 'string', 'Dorime' ),
								),
						),
				)
		)
	);
};


/*
| Returns the note at the current sequence number.
*/
def.lazy.note =
	function( )
{
	const cwList = this._changeWrapList;
	const cwListLen = cwList.length;
	const seq = this.seq;

	if( seq < 0 || seq > cwListLen ) throw new Error( 'invalid seq' );

	// if the requested seq is not latest, rewinds stuff
	let note = this._note;
	for( let a = cwListLen - 1; a >= seq; a-- )
	{
		const changeList = cwList.get( a ).changeList;
		note = changeList.changeTreeReverse( note );
	}
	return note;
};


/*
| The maximum sequence number.
*/
def.lazy.maxSeq = function( ) { return this._changeWrapList.length; };


/*
| Extra checking.
*/
def.proto._check =
	function( )
{
	if( this.seq < 0 || this.seq > this._changeWrapList.length ) throw new Error( );
	if( this.length > 1 ) throw new Error( );
	if( this.length === 1 && this.keys[ 0 ] !== 'note' ) throw new Error( );
};
