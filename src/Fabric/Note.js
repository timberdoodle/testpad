/*
| A fix sized text item.
|
| Has potentionaly a scrollbar.
*/
'use strict';

def.extend = 'Fabric/Item';

def.attributes =
{
	// the notes text
	text: { type: 'Fabric/Text' },
};
