/*
| A sequence of paragraphs.
*/
'use strict';

def.extend = 'Fabric/Fiber';

def.attributes =
{
	// the paragraphs
	paras: { type: [ 'Fabric/Para/List' ] },
};
