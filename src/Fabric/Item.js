/*
| Everything there is in a space.
*/
'use strict';

def.abstract = true;

def.extend = 'Fabric/Fiber';
