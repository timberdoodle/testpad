/*
| A paragraph.
*/
'use strict';

def.extend = 'Fabric/Fiber';

def.attributes =
{
	// the paragraphs string
	string: { type: 'string', json: true },
};
